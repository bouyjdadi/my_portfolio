import { Container, Row, Col, Tab, Nav } from "react-bootstrap";
import { ProjectCard } from "./ProjectCard";
import projImg1 from "../assets/img/projet1.jpeg";
import projImg2 from "../assets/img/projet2.jpeg";
import projImg3 from "../assets/img/projet3.jpeg";
import projImg4 from "../assets/img/projet4.jpeg";
import projImg5 from "../assets/img/projet5.jpeg";
import projImg6 from "../assets/img/projet6.jpeg";
import projImg7 from "../assets/img/projet7.jpeg";
import projImg8 from "../assets/img/projet8.jpeg";
import projImg9 from "../assets/img/projet9.jpeg";
import projImg10 from "../assets/img/projet10.jpeg";
import colorSharp2 from "../assets/img/color-sharp2.png";
import "animate.css";
import TrackVisibility from "react-on-screen";

export const Projects = () => {
  const projects = [
    {
      description: "Design & Development",
      imgUrl: projImg5,
      urlSite: "https://unique-tech-page-k.netlify.app/",
    },
    {
      description: "Design & Development",
      imgUrl: projImg4,
      urlSite: "https://foodies-kawtar.netlify.app/",
    },
    {
      description: "Design & Development",
      imgUrl: projImg6,
      urlSite:"http://pneu-map.com/"
    },
    {
      description: "Design & Development",
      imgUrl: projImg3,
      urlSite: "https://www.almaoun.eu/",
    },
    // {
    //   description: "Design & Development",
    //   imgUrl: projImg1,
    //   urlSite: "http://elnour.byethost7.com/",
    // },
    // {
    //   description: "Design & Development",
    //   imgUrl: projImg2,
    //   urlSite: "http://port.byethost33.com/",
    // },
   
    {
      description: "Design & Development",
      imgUrl: projImg9,
      urlSite: "https://git-engine.vercel.app/",
    },
    {
      description: "Design & Development",
      imgUrl: projImg10,
      urlSite: "https://maison-jungle-two.vercel.app/",
    },
    {
      description: "Design & Development",
      imgUrl: projImg8,
      urlSite:"https://www.bookaser.com/"
    },
    {
      description: "Design & Development",
      imgUrl: projImg7,
      urlSite:"https://kawtar.vercel.app/"
    },
    
  ];

  const projectsFront = [
    {
      description: "Design & Development",
      imgUrl: projImg5,
      urlSite: "https://unique-tech-page-k.netlify.app/",
    },
    {
      description: "Design & Development",
      imgUrl: projImg4,
      urlSite: "https://foodies-kawtar.netlify.app/",
    },
    {
      description: "Design & Development",
      imgUrl: projImg9,
      urlSite:"https://git-engine.vercel.app/"
    },
    {
      description: "Design & Development",
      imgUrl: projImg10,
      urlSite:"https://maison-jungle-two.vercel.app/"
      
    },
    {
      description: "Design & Development",
      imgUrl: projImg7,
      urlSite:"http://kawtar-vercel.app/"
    },
  ];
  const projectsBack = [
    {
      description: "Design & Development",
      imgUrl: projImg6,
      urlSite:"http://pneu-map.com/"
    },
    {
      description: "Design & Development",
      imgUrl: projImg3,
      urlSite: "https://www.almaoun.eu/",
    },   
    {
      description: "Design & Development",
      imgUrl: projImg8,
      urlSite:"https://www.bookaser.com/"
    }
  ];

  return (
    <section className="project" id="project">
      <Container>
        <Row>
          <Col size={12}>
            <TrackVisibility>
              {({ isVisible }) => (
                <div
                  className={
                    isVisible ? "animate__animated animate__fadeIn" : ""
                  }
                >
                  <h2>Projects</h2>
                  <p>
                    I show you to all the big and small websites I have done so
                    far
                  </p>
                  <Tab.Container id="projects-tabs" defaultActiveKey="first">
                    <Nav
                      variant="pills"
                      className="nav-pills mb-5 justify-content-center align-items-center"
                      id="pills-tab"
                    >
                      <Nav.Item>
                        <Nav.Link eventKey="first" className="titreProjet">All projects</Nav.Link>
                      </Nav.Item>
                      <Nav.Item>
                        <Nav.Link eventKey="second" className="titreProjet">Back-End</Nav.Link>
                      </Nav.Item>
                      <Nav.Item>
                        <Nav.Link eventKey="third" className="titreProjet"> Front-End</Nav.Link>
                      </Nav.Item>
                    </Nav>
                    <Tab.Content
                      id="slideInUp"
                      className={
                        isVisible ? "animate__animated animate__slideInUp" : ""
                      }
                    >
                      <Tab.Pane eventKey="first">
                        <Row>
                          {projects.map((project, index) => {
                            return <ProjectCard key={index} {...project} />;
                          })}
                        </Row>
                      </Tab.Pane>
                      <Tab.Pane eventKey="section">
                        <p>
                          Lorem ipsum dolor sit amet consectetur adipisicing
                          elit. Cumque quam, quod neque provident velit, rem
                          explicabo excepturi id illo molestiae blanditiis,
                          eligendi dicta officiis asperiores delectus quasi
                          inventore debitis quo.
                        </p>
                      </Tab.Pane>
                      <Tab.Pane eventKey="second">
                        <Row>
                          {projectsBack.map((project, index) => {
                            return <ProjectCard key={index} {...project} />;
                          })}
                        </Row>
                        {/* <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque quam, quod neque provident velit, rem explicabo excepturi id illo molestiae blanditiis, eligendi dicta officiis asperiores delectus quasi inventore debitis quo.</p> */}
                      </Tab.Pane>
                      <Tab.Pane eventKey="third">
                        <Row>
                          {projectsFront.map((project, index) => {
                            return <ProjectCard key={index} {...project} />;
                          })}
                        </Row>
                        {/* <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque quam, quod neque provident velit, rem explicabo excepturi id illo molestiae blanditiis, eligendi dicta officiis asperiores delectus quasi inventore debitis quo.</p> */}
                      </Tab.Pane>
                    </Tab.Content>
                  </Tab.Container>
                </div>
              )}
            </TrackVisibility>
          </Col>
        </Row>
      </Container>
      <img className="background-image-right" src={colorSharp2}></img>
    </section>
  );
};
